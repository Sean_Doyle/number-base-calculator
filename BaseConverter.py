#Author : Sean Doyle
#Date : 01/11/13

class BaseConverter:
    #converting to and from base 10
    def decToBin(self, x):
		result = ""

		y = int(x)
		if(y == 0):
			result = str(0)

		while y > 0:
			result += str(y%2)
			y = (y/2)
            
		
		return result[::-1]
        
    def binToDec(self, x):
        result = 0
        
        for pos, char in enumerate(x):
            result = result * 2 + int(char) 
        
        return str(result)
    
    def decToOct(self, x):
        result = ""
        
        y = int(x)
        while y >= 8:
            result += str(y%8)
            y = (y/8)
            
        result += str(y%8)
        return str(result[::-1])
        
    def octToDec(self, x):
        result = 0
        
        for pos, char in enumerate(x):
            result = result * 8 + int(char) 
        
        return str(result)
        
    def decToHex(self, x):
        result = ""
        
        y = int(x)
        while y >= 16:
            if y%16 > 9:
                result += self.decIntoHexLetter(y%16)
            else:   
                result += str(y%16)
            
            y = (y/16)
            
        if y%16 > 9:    
            result += self.decIntoHexLetter(y%16)
        else:
            result += str(y%16)
            
        return str(result[::-1])
        
    def hexToDec(self, x):
        result = 0
        
        for pos, char in enumerate(x):
            if self.isHexLetter(char):
                result = result * 16 + self.convertHexLetter(char)
            else:
                result = result * 16 + int(char)
        
        return str(result)

    def decIntoHexLetter(self, x):
        hex_values = {10: "A", 11: "B", 12: "C", 13: "D", 14: "E", 15: "F"}
        return hex_values.get(x, x)
        
            
    def convertHexLetter(self, x):
        hex_values = {"A": 10, "B": 11, "C": 12, "D": 13, "E": 14, "F": 15}
        return hex_values.get(x, x)

            
    def isHexLetter(self, x):
        if x in ("A", "B", "C", "D", "E", "F"):
            return bool(1)
        else:
            return bool(0)
            
    #converting bases into bases other than 10
    def binToOct(self, x):
        return self.decToOct(self.binToDec(x))

    def binToHex(self, x):
        return self.decToHex(self.binToDec(x))
        
    def octToBin(self, x):
        return self.decToBin(self.octToDec(x))
        
    def octToHex(self, x):
        return self.decToHex(self.octToDec(x))

    def hexToBin(self, x):
        return self.decToBin(self.hexToDec(x))
        
    def hexToOct(self, x):
        return self.decToOct(self.hexToDec(x))

