#Author : Sean Doyle
#Date : 01/11/13

from BaseConverter import *
import wx

converter = BaseConverter()

class Gui(wx.Frame):
	numb1 = 0
	numb2 = 0
	
	def	__init__(self):
		wx.Frame.__init__(self, None,wx.ID_ANY, 'Converter', 
			pos=(300,150),size=(350,420),style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER)
		
		
		#Displays
		self.decTitle = wx.StaticText(self,-1,label="Decimal :",pos=(5,0),size=(70,20))
		self.decDisplay = wx.TextCtrl(self,-1,value="",pos=(5,20), size=(325,30))
		
		self.binTitle = wx.StaticText(self,-1,label="Binary :",pos=(5,55),size=(70,20))
		self.binDisplay = wx.TextCtrl(self,-1,value="",pos=(5,75), size=(325,30))
		
		self.octTitle = wx.StaticText(self,-1,label="Octal :",pos=(5,110),size=(70,20))
		self.octDisplay = wx.TextCtrl(self,-1,value="",pos=(5,130), size=(325,30))
		
		self.hexTitle = wx.StaticText(self,-1,label="Hexadecimal :",pos=(5,165),size=(70,20))
		self.hexDisplay = wx.TextCtrl(self,-1,value="",pos=(5,185), size=(325,30))
		
		#Number Buttons
		self.digitOne = wx.Button(self,1,label="1",pos=(5,220),size=(40,40))
		self.digitOne.Bind(wx.EVT_BUTTON, self.DigitOneClick)
		
		self.digitTwo = wx.Button(self,2,label="2",pos=(45,220),size=(40,40))
		self.digitTwo.Bind(wx.EVT_BUTTON, self.DigitTwoClick)
		
		self.digitThree = wx.Button(self,3,label="3",pos=(85,220),size=(40,40))
		self.digitThree.Bind(wx.EVT_BUTTON, self.DigitThreeClick)
			
		self.digitFour = wx.Button(self,4,label="4",pos=(5,260),size=(40,40))
		self.digitFour.Bind(wx.EVT_BUTTON, self.DigitFourClick)
		
		self.digitFive = wx.Button(self,5,label="5",pos=(45,260),size=(40,40))
		self.digitFive.Bind(wx.EVT_BUTTON, self.DigitFiveClick)
		
		self.digitSix = wx.Button(self,6,label="6",pos=(85,260),size=(40,40))
		self.digitSix.Bind(wx.EVT_BUTTON, self.DigitSixClick)
		
		self.digitSeven = wx.Button(self,7,label="7",pos=(5,300),size=(40,40))
		self.digitSeven.Bind(wx.EVT_BUTTON, self.DigitSevenClick)
		
		self.digitEight = wx.Button(self,8,label="8",pos=(45,300),size=(40,40))
		self.digitEight.Bind(wx.EVT_BUTTON, self.DigitEightClick)
		
		self.digitNine = wx.Button(self,9,label="9",pos=(85,300),size=(40,40))
		self.digitNine.Bind(wx.EVT_BUTTON, self.DigitNineClick)
		
		self.digitZero = wx.Button(self,0,label="0",pos=(5,340),size=(40,40))
		self.digitZero.Bind(wx.EVT_BUTTON, self.DigitZeroClick)
		
		self.digitA = wx.Button(self,10,label="A",pos=(140,220),size=(40,40))
		self.digitA.Bind(wx.EVT_BUTTON, self.DigitAClick)

		self.digitB = wx.Button(self,11,label="B",pos=(140,260),size=(40,40))
		self.digitB.Bind(wx.EVT_BUTTON, self.DigitBClick)

		self.digitC = wx.Button(self,12,label="C",pos=(140,300),size=(40,40))
		self.digitC.Bind(wx.EVT_BUTTON, self.DigitCClick)

		self.digitD = wx.Button(self,13,label="D",pos=(180,220),size=(40,40))
		self.digitD.Bind(wx.EVT_BUTTON, self.DigitDClick)

		self.digitE = wx.Button(self,14,label="E",pos=(180,260),size=(40,40))
		self.digitE.Bind(wx.EVT_BUTTON, self.DigitEClick)

		self.digitF = wx.Button(self,15,label="F",pos=(180,300),size=(40,40))
		self.digitF.Bind(wx.EVT_BUTTON, self.DigitFClick)

		#Functionality Buttons
		self.addition = wx.Button(self,16,label="+",pos=(230,220),size=(40,40))
		self.addition.Bind(wx.EVT_BUTTON, self.addClick)

		self.subtraction = wx.Button(self,17,label="-",pos=(270,220),size=(40,40))
		self.subtraction.Bind(wx.EVT_BUTTON, self.subClick)

		self.division = wx.Button(self,18,label="/",pos=(270,260),size=(40,40))
		self.division.Bind(wx.EVT_BUTTON, self.divClick)

		self.multiplication = wx.Button(self,19,label="x",pos=(230,260),size=(40,40))
		self.multiplication.Bind(wx.EVT_BUTTON, self.multClick)

		self.equals = wx.Button(self,20,label="=",pos=(230,300),size=(80,40))
		self.equals.Bind(wx.EVT_BUTTON, self.equalsClick)
		
		self.clear = wx.Button(self,21,label="CLR",pos=(45,340),size=(40,40))
		self.clear.Bind(wx.EVT_BUTTON, self.ClearClick)
		
		self.backspace = wx.Button(self,22,label="DEL",pos=(85,340),size=(40,40))
		self.backspace.Bind(wx.EVT_BUTTON, self.BackspaceClick)
		
		self.Show(True)
			
	def DigitOneClick(self,event):
		self.decDisplay.AppendText("1")
		self.convertDec()

	def DigitTwoClick(self,event):
		self.decDisplay.AppendText("2")
		self.convertDec()
		
	def DigitThreeClick(self,event):
		self.decDisplay.AppendText("3")
		self.convertDec()
		
	def DigitFourClick(self,event):
		self.decDisplay.AppendText("4")
		self.convertDec()

	def DigitFiveClick(self,event):
		self.decDisplay.AppendText("5")
		self.convertDec()
		
	def DigitSixClick(self,event):
		self.decDisplay.AppendText("6")
		self.convertDec()
		
	def DigitSevenClick(self,event):
		self.decDisplay.AppendText("7")
		self.convertDec()
		
	def DigitEightClick(self,event):
		self.decDisplay.AppendText("8")
		self.convertDec()
		
	def DigitNineClick(self,event):
		self.decDisplay.AppendText("9")
		self.convertDec()
		
	def DigitZeroClick(self,event):
		self.decDisplay.AppendText("0")
		self.convertDec()
	
	def DigitAClick(self,event):
		self.hexDisplay.AppendText("A")
		self.convertHex()

	def DigitBClick(self,event):
		self.hexDisplay.AppendText("B")
		self.convertHex()	
		
	def DigitCClick(self,event):
		self.hexDisplay.AppendText("C")	
		self.convertHex()
	
	def DigitDClick(self,event):
		self.hexDisplay.AppendText("D")	
		self.convertHex()
	
	def DigitEClick(self,event):
		self.hexDisplay.AppendText("E")
		self.convertHex()
	
	def DigitFClick(self,event):
		self.hexDisplay.AppendText("F")
		self.convertHex()
	
	def ClearClick(self,event):
		self.decDisplay.Clear()
		self.binDisplay.Clear()
		self.octDisplay.Clear()
		self.hexDisplay.Clear()
		
	def BackspaceClick(self,event):
		num = self.decDisplay.GetValue()
		if(len(num) != 1):
			num = (num[:-1])
			self.decDisplay.Clear()
			self.decDisplay.SetValue(num)
			self.convertDec()
		else:
			self.decDisplay.Clear()
			self.binDisplay.Clear()
			self.octDisplay.Clear()
			self.hexDisplay.Clear()
		
	def convertDec(self):
		num = self.decDisplay.GetValue()
		self.binDisplay.SetValue(converter.decToBin(num))
		self.octDisplay.SetValue(converter.decToOct(num))
		self.hexDisplay.SetValue(converter.decToHex(num))
	
	def convertHex(self):
		num = self.hexDisplay.GetValue()
		self.decDisplay.SetValue(converter.hexToDec(num))
		self.binDisplay.SetValue(converter.hexToBin(num))
		self.octDisplay.SetValue(converter.hexToOct(num))
		
	def addClick(self,event):
		pass
	def subClick(self,event):
		pass
	def multClick(self,event):
		pass
	def divClick(self,event):
		pass
	def equalsClick(self,event):
		pass
		
def main():
	app = wx.PySimpleApp()
	frame = Gui()
	app.MainLoop()

if __name__ == '__main__':
    main()